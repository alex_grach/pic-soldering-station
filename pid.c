#ifndef _4K
#ifndef __CONFIG_H__
#define __CONFIG_H__
#include "config.h"
#endif
#endif

unsigned int PropComp_Tempr=0;
unsigned int IntegComp_Tempr=0;
unsigned int DiffComp_Tempr=0;
unsigned int TotalPID_Tempr=0;

#define PropGain_Tempr 2
#define PropGain_TemprDiv 1
#define IntegGain_Tempr 4
#define IntegGain_TemprDiv 2
#define DiffGain_TemprDiv 4


unsigned char PID(unsigned int settemp, unsigned int currenttemp, unsigned int prevtemp) {
unsigned char PWM;
unsigned int dt, prevdt;

  if (currenttemp <= (settemp - 1)) {
	dt = settemp - currenttemp;
     if (dt > 10)
     	  PWM=MAXPWM;
     else {
         // Calculate Proportional component of PID
         PropComp_Tempr = (dt * PropGain_Tempr) / PropGain_TemprDiv;
         prevdt = settemp - prevtemp;
         // Calculate Integral component of PID
         IntegComp_Tempr = IntegComp_Tempr + ((dt+prevdt / IntegGain_TemprDiv) * IntegGain_Tempr);

         // Since Integral component tends to windup, limit its max value
         if (IntegComp_Tempr > MAXPWM>>1)
         {
             // So manually force the integral component to a max value (obtained during tuning)
             IntegComp_Tempr = MAXPWM>>1;
         }
         
         DiffComp_Tempr = DiffGain_TemprDiv * (dt-prevdt);

         // If P-I control needed:
         TotalPID_Tempr = (PropComp_Tempr + IntegComp_Tempr + DiffComp_Tempr);

         // If P-only control needed:
         //TotalPID_Tempr = PropComp_Tempr;

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//------------------------------------------------------------------------------------
     // Limit the PWM value to 100 if somehow the calculated result of PID value > 100...
     // ...during tuning of Prop/Integral gain defines etc.
     if (TotalPID_Tempr > MAXPWM)
         TotalPID_Tempr = MAXPWM;
     PWM = (char)TotalPID_Tempr;       // Otherwise let it be whatever's calculated
//------------------------------------------------------------------------------------
      }
return PWM;
} 
	else 
return 0x00;
//oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
}
