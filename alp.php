<?php
$dig = "0|1|2|3|4|5|6|7|8|9";
$common = "ls|gt|ps|sp|dash|dot|";
$str = $common."a|b|v|e|i|k|l|m|n|o|p|r|s|t|u|f|h|sh|iu|mz|ya";
$d = explode('|',$dig);
$a = explode('|',$str);
$count = 0;
//$base = 0x57;
$base = 0x11;
$fmt = "\t#define ee_%s \t0x%X \n";
echo "// Digits\n";
foreach ($d as $dig)
	printf($fmt, $dig, $count++);
$count=0;
echo "// RUS\n";
echo "#ifdef _RU\n";
foreach ($a as $al)
	printf($fmt, $al, $base+$count++);
echo "// ENG\n";
echo "#else\n";
$str = $common."a|c|d|e|f|g|h|i|l|m|n|o|p|r|s|t|v|w|x|y";
$a = explode('|',$str);
$count=0;
foreach ($a as $al)
	printf($fmt, $al, $base+$count++);
echo "#endif\n";
?>
