/*
	NOKIA SPFD54124 LCD
 */
 
#ifndef __CONFIG_H__
#define __CONFIG_H__
#include "config.h"
#endif
#ifndef __LCD_H__
#define __LCD_H__
#include "lcd.h"
#endif
#ifndef __EEPROM_H__
#define __EEPROM_H__
#include "eeprom.h"
#endif
#ifndef __PID_H__
#define __PID_H__
#include "pid.h"
#endif
#ifndef __SS_H__
#define __SS_H__
#include "ss.h"
#endif

//clear LCD
void Lcd_Clear(){
unsigned char i,j;
cs=0;
//SetX(0);
//SetY(0);
Lcd_Write(CMD,0x2C);     //RAMWR
	for(i=0;i<132;i++)   
		for(j=0;j<162;j++) {
			
			Lcd_Write_Data(BGC);
			Lcd_Write_Data(BGC);
//			Lcd_Write(DATA,0xFF);
//			Lcd_Write(DATA,0xFF);
		}
	return;
}

// init LCD
void Lcd_Init(void) {
USART_Init();
sclk = 0;
sda = 0;
cs = 0;
rst = 0;
delay_s(2);			// 5mS so says the stop watch(less than 5ms will not work)
rst = 1;
delay_s(2);

Lcd_Write(CMD,0x11);
delay_us(10);
Lcd_Write(CMD,0x3A);          //Interface pixel format:bit1,2,3 
Lcd_Write(DATA,0x55);  	      //
Lcd_Write(CMD,0x36);        //MEMORY DATA ACCESS CONTROL
Lcd_Write(DATA,0x80);	      //MEMORY DATA ACCESS CONTROL
//Lcd_Write(CMD,0x13);          //Normal mode
Lcd_Write(CMD,0x29);          //Display on,0x29=ON,0x28=OFF

cs=1;
//Lcd_Clear(); // clear LCD
return;
}

void SPI_Init(void) {
	SSPEN=1;
}
void USART_Init(void) {
	SYNC=1;
	BRG16=1;
	BRGH=0;
	SPBRGH=0;
	SPBRG=0;
	SPEN=1;
	CSRC=1;
	SREN=0;
	CREN=0;
	SCKP=1;
	TX9=1;
	TXEN=1;
	return;
}


	void Lcd_Write_Data(unsigned char c) {
		TX9D=1;
		TXREG = c | 0x01;
		__asm__("NOP");
		while(!TRMT);
		return;
	}
	
	void Lcd_Write(unsigned char cd, unsigned char c) {
	unsigned char li, rc;
	__asm__ ("clrwdt");	
	rc=0x00;
		if (c & 0x01)
			TX9D=1;
		else
			TX9D=0;
		c >>= 1;
		for(li=0;li<7;li++) {
			rc |= (c & 0x01);
			c >>= 1;
			rc <<= 1;
		}
			rc |= cd;
		while(!TRMT);
		TXREG = rc;
		__asm__("NOP");
		while(!TXIF);
	return;
	}
	
void Lcd_Reinit(unsigned char contrast) {
	contract=0;
}

void SetXY(unsigned char x, unsigned char y, unsigned char dx, unsigned char dy) {
	x+=0x08;
	y+=0x08;
	Lcd_Write(CMD,0x2B);     // x-address
	Lcd_Write(DATA,0x00);
	Lcd_Write(DATA,x);
	Lcd_Write(DATA,0x00);
	Lcd_Write(DATA,x+dx);
	
	Lcd_Write(CMD,0x2A);	// y-address
	Lcd_Write(DATA,0x00);
	Lcd_Write(DATA,y);
	Lcd_Write(DATA,0x00);
	Lcd_Write(DATA,y+dy);
	Lcd_Write(CMD,0x2C);
return;
}