/* ----------------------------------------------------------------------- */
/* --------------- PIC16F887 soldering station with hot air -------------- */
/* --------------------- Author: Alexey Grachov -------------------------- */
/* ---------------------------- gav@bmstu.ru ----------------------------- */
/* ----------------------------------------------------------------------- */
#ifndef __CONFIG_H__
#define __CONFIG_H__
#include "config.h"
#endif
#ifndef __LCD_H__
#define __LCD_H__
#include "lcd.h"
#endif
#ifndef __EEPROM_H__
#define __EEPROM_H__
#include "eeprom.h"
#endif
#ifndef __PID_H__
#define __PID_H__
#include "pid.h"
#endif
#ifndef __SS_H__
#define __SS_H__
#include "ss.h"
#endif

void PWM_On(void);
void PWM_Off(void);
unsigned int ADC_Get(unsigned char ch);

void ON_OFF(unsigned char selected, unsigned char y, unsigned char width);

typedef unsigned int word;
#ifdef		PIC16F88x
word __at _CONFIG1 CONFIG1 = _INTRC_OSC_NOCLKOUT & _WDT_ON & _PWRTE_ON & _MCLRE_OFF & _CP_OFF & _CPD_OFF & _BOR_ON & _IESO_OFF & _FCMEN_OFF & _LVP_OFF & _DEBUG_OFF;
word __at _CONFIG2 CONFIG2 = _BOR21V & _WRT_OFF;
#endif
#ifdef		PIC16F91x
word __at 0x2007 CONFIG = _INTRC_OSC_NOCLKOUT & _WDT_ON & _PWRTE_ON & _MCLRE_OFF & _CP_OFF & _CPD_OFF & _IESO_OFF & _FCMEN_OFF & _DEBUG_OFF;
#endif
#ifdef		PIC16F87x
word __at 0x2007 CONFIG = _HS_OSC & _WDT_ON & _PWRTE_ON & _BODEN_ON & _LVP_OFF & _CPD_OFF & _WRT_ENABLE_OFF & _DEBUG_OFF & _CP_OFF;
#endif
#ifdef		PIC16F87xa
word __at 0x2007 CONFIG = _HS_OSC & _WDT_ON & _PWRTE_ON & _BODEN_ON & _LVP_OFF & _CPD_OFF & _WRT_OFF & _DEBUG_OFF & _CP_OFF;
#endif

typedef unsigned char eeprom; 
__code eeprom __at 0x2100 __EEPROM[] = { 
0x3E, 0x51, 0x49, 0x45, 0x3E,// 0
0x00, 0x42, 0x7F, 0x40, 0x00,// 1
0x42, 0x61, 0x51, 0x49, 0x46,// 2
0x21, 0x41, 0x45, 0x4B, 0x31,// 3
0x18, 0x14, 0x12, 0x7F, 0x10,// 4
0x27, 0x45, 0x45, 0x45, 0x39,// 5
0x3C, 0x4A, 0x49, 0x49, 0x30,// 6
0x01, 0x71, 0x09, 0x05, 0x03,// 7
0x36, 0x49, 0x49, 0x49, 0x36,// 8
0x06, 0x49, 0x49, 0x29, 0x1E,// 9
0x00, 0x00, 0x00, 0x00, 0x00,// пробел
0x00, 0x08, 0x08, 0x08, 0x00,
// Stored temperature values
0x68, 0x01, 0x1C, 0x02, 0x32,

// write VOP register __EEPROM[41]
#ifdef _3V 
0x20,
#else
0x21,
#endif
0x80,
0xA4, // all on/normal display
0x2F, // Power control set(charge pump on/off)
0x40, // set start row address = 0
0xB0, // set Y-address = 0
0x10, // set X-address, upper 3 bits
0x00,  // set X-address, lower 4 bits
#ifdef _INV
0xC8, // mirror Y axis (about X axis)
#else
0xC0, // !mirror Y axis (about X axis)
#endif
0xA0, // !Invert screen in horizontal axis
//0xA1, // Invert screen in horizontal axis
0xAC, // set initial row (R0) of the display
0x00,
//Lcd_Write(CMD,0xF9); // 
0xAF, // display ON/OFF

#ifdef _RU
// <уст>
// < 
0x00, 0x08, 0x1C, 0x3E, 0x7F, 0x00,
// 0x1D - 'У'.
0x27, 0x48, 0x48, 0x48, 0x3f, 0x00,
// 0x1B - 'С'.
0x3e, 0x41, 0x41, 0x41, 0x22, 0x00,
// 0x1C - 'Т'.
0x01, 0x01, 0x7f, 0x01, 0x01, 0x00,
// >
0x00, 0x7F, 0x3E, 0x1C, 0x08, 0x00,
// <тек>
// < 
0x00, 0x08, 0x1C, 0x3E, 0x7F, 0x00,
// 0x1C - 'Т'.
0x01, 0x01, 0x7f, 0x01, 0x01, 0x00,
// 0xf - 'Е'.
0x7f, 0x49, 0x49, 0x49, 0x41, 0x00,
// 0x14 - 'К'.
0x7f, 0x08, 0x14, 0x22, 0x41, 0x00,
// >
0x00, 0x7F, 0x3E, 0x1C, 0x08, 0x00,

// 0x19 - 'П'
0x7f, 0x01, 0x01, 0x01, 0x7f, 0x00,
// 0xa - 'А'
0x7e, 0x11, 0x11, 0x11, 0x7e, 0x00,
// 0x29 - 'Я'
0x66, 0x19, 0x09, 0x09, 0x7f, 0x00,
// 0x15 - 'Л'
0x40, 0x7e, 0x01, 0x01, 0x7f, 0x00,
// 0x26 - 'Ь'
0x7f, 0x48, 0x48, 0x48, 0x30, 0x00,
// 0x17 - 'Н'
0x7f, 0x08, 0x08, 0x08, 0x7f, 0x00,
// 0x12 - 'И'
0x7f, 0x20, 0x10, 0x08, 0x7f, 0x00,
// 0x14 - 'К'
0x7f, 0x08, 0x14, 0x22, 0x41, 0x00,

// 0x1E - 'Ф'
0x0e, 0x11, 0x7f, 0x11, 0x0e, 0x00,
// 0xf - 'Е'
0x7f, 0x49, 0x49, 0x49, 0x41, 0x00,
// 0x17 - 'Н'
0x7f, 0x08, 0x08, 0x08, 0x7f, 0x00,

// 0x18 - 'О'.
0x3e, 0x41, 0x41, 0x41, 0x3e, 0x00,
// 0xb - 'Б'.
0x7f, 0x49, 0x49, 0x49, 0x31, 0x00,
// 0x18 - 'О'.
0x3e, 0x41, 0x41, 0x41, 0x3e, 0x00,
// 0x1A - 'Р'.
0x7f, 0x09, 0x09, 0x09, 0x06, 0x00,
// 0x18 - 'О'.
0x3e, 0x41, 0x41, 0x41, 0x3e, 0x00,
// 0x1C - 'Т'.
0x01, 0x01, 0x7f, 0x01, 0x01, 0x00,
// 0x25 - 'Ы'.
0x7f, 0x48, 0x48, 0x30, 0x7f, 0x00,
// % 
0x23, 0x13, 0x08, 0x64, 0x62, 0x00,
#else
// <set>
// < 
0x00, 0x08, 0x1C, 0x3E, 0x7F, 0x00,
// 0x1D - 'S'.
0x46, 0x49, 0x49, 0x49, 0x31, 0x00,
// 0x1B - 'E'.
0x7f, 0x49, 0x49, 0x49, 0x41, 0x00,
// 0x1C - 'T'.
0x01, 0x01, 0x7f, 0x01, 0x01, 0x00,
// >
0x00, 0x7F, 0x3E, 0x1C, 0x08, 0x00,
// <get>
// < 
0x00, 0x08, 0x1C, 0x3E, 0x7F, 0x00,
// 0x1C - 'G'.
0x3E, 0x41, 0x41, 0x51, 0x32, 0x00,
// 0xf - 'E'.
0x7f, 0x49, 0x49, 0x49, 0x41, 0x00,
// 0x14 - 'T'.
0x01, 0x01, 0x7f, 0x01, 0x01, 0x00,
// >
0x00, 0x7F, 0x3E, 0x1C, 0x08, 0x00,

// 0x19 - 'I'
0x00, 0x41, 0x7F, 0x41, 0x00, 0x00,
// 0xa - 'R'
0x7F, 0x09, 0x19, 0x29, 0x46, 0x00,
// 0x29 - 'O'
0x3e, 0x41, 0x41, 0x41, 0x3e, 0x00,
// 0x15 - 'N'
0x7F, 0x04, 0x08, 0x10, 0x7F, 0x00,

// 0x26 - 'H'
0x7F, 0x08, 0x08, 0x08, 0x7F, 0x00,
// 0x29 - 'O'
0x3e, 0x41, 0x41, 0x41, 0x3e, 0x00,
// 0x12 - 'T'
0x01, 0x01, 0x7f, 0x01, 0x01, 0x00,
// 0x14 - 'A'
0x7E, 0x11, 0x11, 0x11, 0x7E, 0x00,
// 0x1E - 'I'
0x00, 0x41, 0x7F, 0x41, 0x00, 0x00,
// 0xa - 'R'
0x7F, 0x09, 0x19, 0x29, 0x46, 0x00,

// 0x18 - 'S'.
0x46, 0x49, 0x49, 0x49, 0x31, 0x00,
// 0x1A - 'Р'.
0x7F, 0x09, 0x09, 0x09, 0x06, 0x00,
// 0x18 - 'E'.
0x7f, 0x49, 0x49, 0x49, 0x41, 0x00,
// 0x1C - 'E'.
0x7f, 0x49, 0x49, 0x49, 0x41, 0x00,
// 0x25 - 'D'.
0x7F, 0x41, 0x41, 0x22, 0x1C, 0x00,

// % 
0x23, 0x13, 0x08, 0x64, 0x62, 0x00,
// % 
0x23, 0x13, 0x08, 0x64, 0x62, 0x00,
// % 
0x23, 0x13, 0x08, 0x64, 0x62, 0x00,
// % 
0x23, 0x13, 0x08, 0x64, 0x62, 0x00,
#endif
0x64,  // Fan off temp
0x05, // Iron first heat PWM
};

typedef union {
	struct {
	  unsigned char PWM:1;
	  unsigned char KEYPRESSED:1;
	  unsigned char SLEEP:1;
	  unsigned char SAVE:1;
	  unsigned char CHECK:1;
	  unsigned char SHOW:1;
	  unsigned char HOTAIR:1;
	  unsigned char IRON:1;
	};
} __SS_bits_t;
__SS_bits_t SS_bits;

#define SAVE		SS_bits.SAVE
#define SHOW		SS_bits.SHOW
#define CHECK		SS_bits.CHECK
#define HOTAIR		SS_bits.HOTAIR
#define IRON		SS_bits.IRON
#define SLEEP		SS_bits.SLEEP
#define KEYPRESSED      SS_bits.KEYPRESSED
#define PWM		SS_bits.PWM

unsigned char i, j, k, dec, bt_delay, on_off_delay, fan_speed, fan_defaults, sec, min, hour, pwm=0, pwm_fan=0;

unsigned int itemp=0; // Температура паяльника
unsigned int itempprev=0; // Предыдущее показание температура паяльника
unsigned int itempav=0; // Усредненная температура паяльника
unsigned int hatemp=0; // Температура фена
unsigned int hatempav=0; // Усредненная температура фена
unsigned int prog[2]; // Текушие установки температуры
unsigned int defaults[2]; // Сохраненные значения температуры из EEPROM
unsigned int fan_off; // Температура выключения фена при остывании
unsigned int iron_correction; // Коррекция температуры паяльника в покое (ШИМ)


void isr() __interrupt 0 {
//	GIE=0;
		if (TMR2IF) {
			CCPR1L=pwm;
			CCPR2L=pwm_fan;
			TMR2IF=0;

			__asm__ ("CLRWDT");
		}

		if (TMR1IF) {

			TMR1IF=0;
			CHECK=1;

			if (on_off_delay) on_off_delay--;
			if (bt_delay) bt_delay--;
			
			itempav += itemp;
			itempav >>= 1;
			hatempav += hatemp;
			hatempav >>= 1;

			if (sec++>15) {
				sec=0;
				min--;
				if (!min) {
					min=0xCC;
					hour--;
					if (!hour) SLEEP=1;
				}
				SHOW=1;
			}

			if (!bt_delay) {
           if(BUTTONS!=0xFF) {
           	bt_delay=(!KEYPRESSED)?10:1;
           	KEYPRESSED=1;
           	
				if (!IP) {
					if (prog[0]>IronMax-2)
						prog[0]=IronMax;
					else
						prog[0]+=2;
				}
				if (!IM) {
					if (prog[0]<IronMin+2)
						prog[0]=0;
					else
						prog[0]-=2;
				}
				if (!HAP) {
					if (prog[1]>HotAirMax-2)
						prog[1]=HotAirMax;
					else
						prog[1]+=2;
				}
				if (!HAM) {
					if (prog[1]<HotAirMin+2)
						prog[1]=0;
					else
						prog[1]-=2;
				}
				if (!FP) {
					if (fan_speed>FanMax-1)
						fan_speed=FanMax;
					else
						fan_speed++;
				}
				if (!FM) {
					if (fan_speed<FanMin+1)
						fan_speed=0;
					else
						fan_speed--;
				}				
           SHOW=1;
			} else KEYPRESSED=0;
       }
	}

		if (RBIF) {
         RBIF=0;
			if (SLEEP) {
				min=0xCC;
				hour=0x78;
				SLEEP=0;
			} else {
				if (!on_off_delay && BUTTONS!=0xFF) {
					if (!ION) {
						IRON=!IRON;
					}
					if (!HAON) {
						HOTAIR=!HOTAIR;
					}
					if (!IRON || !HOTAIR) 
						if (defaults[0]!=prog[0]) SAVE=1;
						if (defaults[1]!=prog[1]) SAVE=1;
						if (fan_defaults!=fan_speed) SAVE=1;
				on_off_delay=15;
				}
			SHOW=1;
			}
		}	
//	GIE=1;
}

void main() {
unsigned char * pt;

	SLEEP=1;

	PORTB=0;
	PORTC=0;
	TRISC=0;
#ifdef _LEGACY
	PORTD=0;
	TRISD=0;
	PORTE=0;
	TRISE=0;
#endif

#ifdef PIC16F88x
	WDTCON=0x0A;
	ANSELH=0;
	IOCB=WPUB=0xFF;
	hotAirSwitchAN=0;
#endif
#ifdef PIC16F91x
	WDTCON=0x0A;
	IOCB=WPUB=0xFF;
#endif
#if defined(PIC16F87x) || defined(PIC16F87xa)
	PCFG2=1;
#endif


//	TRISB=0xFF;	//PORTB<0:5> input
	NOT_RBPU=0;	// Enable pull-ups

	ADFM=1;
	hotAirSwitchT=1;
	ironT=hotAirT=1;
	ironA=hotAirA=1;
	Lcd_Init();
	
	for (i=0;i<4;i++) {
		pt = (unsigned char *)(&prog[0])+i;
		*pt = EERead(eeprog+i);
		defaults[0] = *pt;
	}
	fan_defaults=fan_speed=EERead(eeprog+i);
	fan_off=EERead(0xFC);
	iron_correction=EERead(0xFD);

	// УСТ
	EEWriteMenu(2*WIDTH+4,0,0x4E,5);
	// ТЕК
	EEWriteMenu(10*WIDTH+4,0,0x6B,5);
#ifdef _RU
	EEWriteMenu(WIDTH,HEIGHT,0x89,8);
	EEWriteMenu(WIDTH,4*HEIGHT,0xB9,3);
	EEWriteMenu(WIDTH,7*HEIGHT,0xCB,7);
	EEWriteMenu(15*WIDTH,7*HEIGHT,0xF5,1);
#else
	// IRON
	EEWriteMenu(WIDTH,HEIGHT,0x89,4);
	// HOTAIR
	EEWriteMenu(WIDTH,4*HEIGHT,0xA1,6);
	// SPEED
	EEWriteMenu(WIDTH,7*HEIGHT,0xC5,5);
	EEWriteMenu(15*WIDTH,7*HEIGHT,0xF5,1);
#endif
	//Interrupts enable bits
	PEIE=1;
	TMR1IE=1;
	TMR2IE=1;
	RBIE=1;

	TMR1IF=TMR2IF=RBIF=0;

	T1CON=0x01; // Timer1 prescaler=1:1 on	
	T2CON=0x7F; // Timer2 prescaler=1:16 postscaler=1:16 on
//	T2CON=0x7C; // Timer2 prescaler=1:1 postscaler=1:16 on

	bt_delay=sec=0;
	IRON=HOTAIR=SAVE=0;
	SHOW=1;
// PWM module configuration IRON
	ironTPWM=1;
	PR2=0xFF;
//	PR2=0x65;
//	CCP1CON=0;
//	CCPR1L=0;
	ironTPWM=0;
// PWM module configuration FAN
//	fanT=1;
//	CCP2CON=0;
//	CCPR2L=(fan_speed<<1)+0x37;
	fanT=0;
//	TRISC1=0;
//	TMR1IF=0;
//	TMR1IE=1;
	CCPR1L=0;
	CCP1CON=0x0C;
	CCPR2L=0;
	CCP2CON=0x0F;
	GIE=1;
//	led=!led;
	while (1) {
		
			__asm__ ("CLRWDT");

		if (CHECK) {
				// Iron
				itempprev=itemp;
				itemp=ADC_Get(ironAD);
//				itemp>>=1;
				if (IRON) {
					if (itemp<40) pwm=MAXPWM>>1;
					else pwm = PID(prog[0], itemp, itempprev);
/*					if (itemp>prog[0]) CCPR1L>>=1;
					else if (itemp>prog[0]-10) {
						if (itemp==prog[0]) ;
						else if (itemp>itempprev) {
							if ((itemp-itempprev)>1) CCPR1L>>=1;
							else if (CCPR1L) CCPR1L--; 
							else CCPR1L=0xFF;
						}
						else if ((itempprev-itemp)>1) CCPR1L<<=1;
						else if (CCPR1L!=0xFF) CCPR1L++;
					}
					else if (CCPR1L!=0xFF) CCPR1L++; */
				} else {
					pwm=0;
				}

				// HotAir
				hatemp=ADC_Get(hotAirAD);
//				hatemp>>=1;
				if (HOTAIR && hotAirSwitch) {
					if (hatemp>prog[1]) hotAir=0; else hotAir=1;
					pwm_fan=(fan_speed<<1)+0x37;
//					CCPR2L=(fan_speed<<1)+0x37;
//					CCP2CON=0x0F;
				} else {
					hotAir=0;
					//pwm_fan=0xFF;
					//CCPR2L=0xFF;
					if (hatemp<fan_off) { pwm_fan=0; }
					else if (hatemp>fan_off+20) pwm_fan=MAXPWM;
				}
		CHECK=0;
		}
		if (SHOW) {
			if (SLEEP) {
				IRON=0;
				HOTAIR=0;
				led=0;
			}
			else led=1;
			// Iron
			EE_Dec(prog[0]>>1,0,2);
//			EE_Dec(itemp>>1,4,2);
			EE_Dec(itempav>>1,4,2);
			#ifdef _RU
				ON_OFF(IRON, 1, 9);
			#else
				ON_OFF(IRON, 1, 5);
			#endif
			// Hot Air
			EE_Dec(prog[1]>>1,0,5); 
//			EE_Dec(hatemp>>1,4,5);
			EE_Dec(hatempav>>1,4,5);
			#ifdef _RU
				ON_OFF(HOTAIR, 4, 4);
			#else
				ON_OFF(HOTAIR, 4, 7);
			#endif
			// Fan speed
			EE_Dec_Small(fan_speed,12,7);
//	EE_Dec(1,0);
		SHOW=0;
//		led=!led;
				if (SAVE) {
					for (i=0;i<4;i++) {
						EEWrite( (unsigned char *)(&prog[0])+i, (eeprog+i));
					}
					EEWrite(&fan_speed, eeprog+4);
					SAVE=0;
				}
		}
	}
} 

void EEWriteMenu(unsigned char x, unsigned char y, unsigned char msg, unsigned char len) {
	SetX(x);
	SetY(y);
	for (i=0;i<6*len;i++)
		Lcd_Write(DATA,EERead(msg+i));
	return;
}

void ON_OFF (unsigned char selected, unsigned char y, unsigned char w) {
unsigned char sym;
	SetX(0);
	SetY(y);
	for (i=0; i<6; i++) {
		if (selected) sym=EERead(0x65+i); else sym=0x00;
		Lcd_Write(DATA, sym);
	}
	SetX(WIDTH*w);
	for (i=0; i<6; i++) {
		if (selected) sym=EERead(0x4E+i); else sym=0x00;
		Lcd_Write(DATA,sym);
	}
	return;
}

void delay_s(unsigned char count) {
unsigned char i,j,k;

    for (i=0;i<count;i++) {
     __asm__ ("clrwdt");
		for (j=0;j<100;j++)
			for (k=0;k<100;k++);
    }
    return;
}

void delay_us(unsigned char count) {
unsigned char i;

	for (i=0;i<count;i++) ;
	return;
}


unsigned int ADC_Get(unsigned char cfg) {
unsigned int res;
//unsigned char ipwm, fpwm;
//	ipwm = CCP1CON;
//	fpwm = CCP2CON;
//	CCP1CON=CCP2CON=0;
	ADCON0 = cfg;
	delay_us(10);
	ADON=1;
	delay_us(10);
	GO_DONE=1;	
	while(GO_DONE);
	ADON=0;
//	CCP1CON=ipwm;
//	CCP2CON=fpwm;
	res=ADRESH;
	res<<=8;
	res+=ADRESL;
	return res;
}