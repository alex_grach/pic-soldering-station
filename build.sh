#!/bin/bash
#set -x
# ss firmware build script
#cpu=( 16f876a )
cpu_4k=( 16f884 16f883 )
cpu=( 16f887 16f886 16f877 16f876 16f877a 16f876a )
langs=( _RU _EN )
voltage=( _5V _3V )
hide=( _SHOW _HIDE )
invert=( _NORM _INV )

if [ $# -gt 0 ]; then
	for args in $@
	do	
	case ${args} in
		clean)
			rm *.lst *.cod *.map *.cof *.o *.asm *.adb *.hex ;
			echo "Project cleaned"
			;;
		proteus)
			echo "Deleting old firmware..."
			rm *.hex *.zip &&
			echo "Done!"
			# Симулятор
			c=16f876a
			o=" -D_RU -D_5V -D_SHOW -D_INV"
					echo ${c} 
					echo "Starting legacy build..."
					sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -o ss.asm ss.c &&
					gpasm -a inhx8m -c ss.asm &&
					sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -o lcd.asm lcd.c &&
					gpasm -a inhx8m -c lcd.asm &&
					sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -o eeprom.asm eeprom.c &&
					gpasm -a inhx8m -c eeprom.asm &&
					sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -o pid.asm pid.c &&
					gpasm -a inhx8m -c pid.asm &&
					gplink -c -m -w -r -I/usr/local/share/sdcc/lib/pic14 -I/usr/local/share/sdcc/non-free/lib/pic14 -s /usr/local/share/gputils/lkr/${c}_g.lkr -o `echo ss_${c}_proteus.hex | tr '[:upper:]' '[:lower:]'` ss.o lcd.o eeprom.o pid.o libsdcc.lib pic${c}.lib &&
					rm *.lst *.cod *.map *.cof &&
					cp ./ss_${c}_proteus.hex ~/Downloads/ss_876/ss.hex
					cp ./*.asm ~/Downloads/ss_876/
					echo "Building `echo ss_${c}_proteus.hex | tr '[:upper:]' '[:lower:]'` finished!"
			;;
		legacy)
			echo "Deleting old firmware..."
			rm *.hex *.zip &&
			echo "Done!"
			# Сапоги для сапожника )))
			c=16f887
			o=" -D_RU -D_5V -D_SHOW -D_NORM -D_LEGACY"
					echo ${c} 
					echo "Starting legacy build..."
					sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -o ss.asm ss.c &&
					gpasm -a inhx8m -c ss.asm &&
					sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -o lcd.asm lcd.c &&
					gpasm -a inhx8m -c lcd.asm &&
					sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -o eeprom.asm eeprom.c &&
					gpasm -a inhx8m -c eeprom.asm &&
					sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -o pid.asm pid.c &&
					gpasm -a inhx8m -c pid.asm &&
					gplink -c -m -w -r -I/usr/local/share/sdcc/lib/pic14 -I/usr/local/share/sdcc/non-free/lib/pic14 -s /usr/local/share/gputils/lkr/${c}_g.lkr -o `echo ss_${c}${l}${v}${h}${i}.hex | tr '[:upper:]' '[:lower:]'` ss.o lcd.o eeprom.o pid.o libsdcc.lib pic${c}.lib &&
					rm *.lst *.cod *.map *.cof &&
					echo "Building `echo ss_${c}_legacy.hex | tr '[:upper:]' '[:lower:]'` finished!"
			;;
		build)
			echo "Deleting old firmware..."
			rm *.hex &&
			echo "Done!"
			for c in ${cpu[@]}
			do
				for l in ${langs[@]}
				do
					for v in ${voltage[@]}
					do
						for h in ${hide[@]}
						do
							for i in ${invert[@]}
							do
								echo ${c} 
								echo "Starting build..."
								sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` -D${l} -D${v} -D${h} -D${i} -o ss.asm ss.c &&
								gpasm -a inhx8m -c ss.asm &&
								sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` -D${l} -D${v} -D${h} -D${i} -o lcd.asm lcd.c &&
								gpasm -a inhx8m -c lcd.asm &&
								sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` -D${l} -D${v} -D${h} -D${i} -o eeprom.asm eeprom.c &&
								gpasm -a inhx8m -c eeprom.asm &&
								sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` -D${l} -D${v} -D${h} -D${i} -o pid.asm pid.c &&
								gpasm -a inhx8m -c pid.asm &&
								gplink -c -m -w -r -I/usr/local/share/sdcc/lib/pic14 -I/usr/local/share/sdcc/non-free/lib/pic14 -s /usr/local/share/gputils/lkr/${c}_g.lkr -o `echo ss_${c}${l}${v}${h}${i}.hex | tr '[:upper:]' '[:lower:]'` ss.o lcd.o eeprom.o pid.o libsdcc.lib pic${c}.lib &&
								rm *.lst *.cod *.map *.cof &&
								echo "Building `echo ss_${c}${l}${v}${h}${i}.hex | tr '[:upper:]' '[:lower:]'` finished!"
							done
						done
					done
				done
			done
			
			echo "Creating archive..."
			zip -9 ss.zip *.c *.h *.hex *.sh &&
			echo "Done!"
			;;
		build_4k)
			echo "Deleting old firmware..."
			rm *.hex &&
			echo "Done!"
			for c in ${cpu_4k[@]}
			do
				for l in ${langs[@]}
				do
					for v in ${voltage[@]}
					do
						for h in ${hide[@]}
						do
							for i in ${invert[@]}
							do
								echo ${c} 
								echo "Starting build..."
								sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` -D${l} -D${v} -D${h} -D${i} -o ss.asm ss.c &&
								gpasm -a inhx8m -c ss.asm &&
			#					cat lcd.c eeprom.c pid.c > functions.c &&
			#					sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` -D${l} -D${v} -D${h} -D${i} -o functions.asm functions.c &&
								sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` -D${l} -D${v} -D${h} -D${i} -o lcd.asm lcd.c &&
								gpasm -a inhx8m -c functions.asm &&
								gplink -c -m -w -r -I/usr/local/share/sdcc/lib/pic14 -I/usr/local/share/sdcc/non-free/lib/pic14 -s /usr/local/share/gputils/lkr/${c}_g.lkr -o `echo ss_${c}${l}${v}${h}${i}.hex | tr '[:upper:]' '[:lower:]'` ss.o functions.o libsdcc.lib pic${c}.lib &&
								rm *.lst *.cod *.map *.cof &&
								echo "Building `echo ss_${c}${l}${v}${h}${i}.hex | tr '[:upper:]' '[:lower:]'` finished!"
							done
						done
					done
				done
			done
			
			echo "Creating archive..."
			zip -9 ss_4k.zip *.c *.h *.hex *.sh &&
			echo "Done!"
			;;
		
	esac
	done
else
	echo "No option specified!"
	echo "Usage : ./build.sh [options]"
	echo "Options :"
	echo ""
	echo "	build - make firmware for"
	for c in  ${cpu[@]}
	do
		echo "		${c}"
	done
	echo "			MCUs"
	echo ""
	echo "	build_4k - make firmware for"
	for c in  ${cpu_4k[@]}
	do
		echo "		${c}"
	done
	echo "			MCUs"
	echo ""
	echo "	clean - cleanup build files"
	echo ""
fi

