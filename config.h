#ifndef CPU
#define PIC16F876A // Тип МК
#define _RU
#define _3V
#endif

#ifndef _RU
//#define _RU // Язык интерфейса
#endif

#ifndef _EN
//#define _EN // Язык интерфейса
#endif

#ifndef _INV
//#define DEBUG
#endif

#ifndef _HIDE
//#define HIDE // Не показывать T<50
#endif

#ifndef _3V
//#define _3V // Напряжение питания LCD
#endif
#ifndef _5V
//#define _5V // Напряжение питания LCD
#endif

#ifdef PIC16F887
#define PIC16F88x
#include <pic16f887.h>
#endif

#ifdef PIC16F886
#define PIC16F88x
#include "pic16f886.h"
#endif

#ifdef PIC16F884
#define PIC16F88x
#define _4K
#include "pic16f884.h"
#endif

#ifdef PIC16F883
#define PIC16F88x
#define _4K
#include "pic16f883.h"
#endif

#ifdef PIC16F877
#include "pic16f877.h"
#define PIC16F87x
#endif

#ifdef PIC16F876
#include "pic16f876.h"
#define PIC16F87x
#endif

#ifdef PIC16F874
#define _4K
#include "pic16f874.h"
#define PIC16F87x
#endif

#ifdef PIC16F873
#define _4K
#include "pic16f873.h"
#define PIC16F87x
#endif

#ifdef PIC16F877A
#include "pic16f877a.h"
#define PIC16F87xa
#endif

#ifdef PIC16F876A
#include "pic16f876a.h"
#define PIC16F87xa
#endif

#ifdef PIC16F874A
#define _4K
#include "pic16f874a.h"
#define PIC16F87xa
#endif

#ifdef PIC16F873A
#define _4K
#include "pic16f873a.h"
#define PIC16F87xa
#endif

#ifdef PIC16F946
#include "pic16f946.h"
#define PIC16F91x
#endif

#ifdef PIC16F917
#include "pic16f917.h"
#define PIC16F91x
#endif

#ifdef PIC16F914
#define _4K
#include "pic16f914.h"
#define PIC16F91x
#endif

#ifdef PIC16F1789
#include "pic16f1789.h"
#define PIC16F178x
#endif

#ifdef PIC16F1788
#include "pic16f1788.h"
#define PIC16F178x
#endif

#ifdef PIC16F1787
#include "pic16f1787.h"
#define PIC16F178x
#endif

#ifdef PIC16F1786
#include "pic16f1786.h"
#define PIC16F178x
#endif

#ifdef PIC16F1939
#include "pic16f1939.h"
#define PIC16F193x
#endif

#ifdef PIC16F1938
#include "pic16f1938.h"
#define PIC16F193x
#endif

#ifdef PIC16F1937
#include "pic16f1937.h"
#define PIC16F193x
#endif

#ifdef PIC16F1936
#include "pic16f1936.h"
#define PIC16F193x
#endif

#ifdef _LEGACY
// set output pins for lcd here
	#define rst 	RC6
	#define cs 	RC5
	#define sda 	RD3
	#define sclk 	RD2
	#define led	RC3

	#define ironA	ANS6
	#define ironT	TRISE1
	#define ironAD	0x98

	#define hotAirA	ANS5
	#define hotAirT	TRISE0
	#define hotAirAD	0x94
	#define hotAirSwitchAN	ANS2

	#define BUTTONS PORTB
	#define IP	RB0
	#define IM	RB5
	#define HAP	RB2
	#define HAM	RB3
	#define FP	RB4
	#define FM	RB1
	#define ION	RB7
	#define HAON	RB6

#else

	#define rst 	RC7
	#define cs 	RC6
	#define sda 	RC5
	#define sclk 	RC4
	#define led	RC3
	
	#define ironA	RA0
	#define ironT	TRISA0
	#define ironAD	0x80
	
	#define hotAirA	RA1
	#define hotAirT	TRISA1
	#ifdef PIC16F88x
		#define hotAirAD	0x84
		#define hotAirSwitchAN	ANS2
	#else
		#define hotAirAD	0x88
	#endif

	#define BUTTONS PORTB
	#define IP	RB0
	#define IM	RB1
	#define HAP	RB2
	#define HAM	RB3
	#define FP	RB4
	#define FM	RB5
	#define ION	RB6
	#define HAON	RB7
	
#endif

#define iron	RC2
#define ironTPWM	TRISC2

#define hotAir	RC0
#define hotAirSwitch	RA2
#define hotAirSwitchT	TRISA2
	
#define fanT	TRISC1
#define fan	RC1
	
#define eelcd	0x41
#define eeprog	0x3C

#define IronMax 800
#define IronMin 0
#define HotAirMax 1000
#define HotAirMin 0
#define FanMax 100
#define FanMin 0

// PWM limits
#define MAXPWM 0xFE

// set output pins for lcd here

#define CMD	0
#define DATA	1

#define WIDTH	6
#define HEIGHT	1
#define MID_WIDTH	12
#define MID_HEIGHT	2
