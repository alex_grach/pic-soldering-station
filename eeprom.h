void EE_Char(unsigned char eesym, unsigned char x, unsigned char y);
void EE_Dec(unsigned int val, unsigned char x, unsigned char y);
void EE_Dec_Small(unsigned char val, unsigned char x, unsigned char y);
unsigned char EERead(unsigned char addr);
void EEReadInt(unsigned int * val, unsigned char adr);
void EEWrite(unsigned char * val, unsigned char adr);
void EE_Char_Mid(unsigned char eesym, unsigned char x, unsigned char y);
void EE_Char_Small(unsigned char eesym, unsigned char x, unsigned char y);
void EEWriteMenu(unsigned char x, unsigned char y, unsigned char msg, unsigned char len);

