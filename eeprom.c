#ifndef _4K
#ifndef __CONFIG_H__
#define __CONFIG_H__
#include "config.h"
#endif
#ifndef __LCD_H__
#define __LCD_H__
#include "lcd.h"
#endif
#ifndef __EEPROM_H__
#define __EEPROM_H__
#include "eeprom.h"
#endif
#endif

unsigned char conv[16] = {0x00, 0x03, 0x0C, 0x0F, 0x30, 0x33, 0x3C, 0x3F, 0xC0, 0xC3, 0xCC, 0xCF, 0xF0, 0xF3, 0xFC, 0xFF}; // 10x14

unsigned char EERead(unsigned char adr) {
	EEADR=adr;
	RD=1;
	return EEDATA;
}

void EEWrite(unsigned char * val, unsigned char adr) {
	EEADR=adr;
	EEDATA=*val;
	EEPGD=0;
	EEIF=0;
	WREN=1;
	EECON2=0x55;
	EECON2=0xAA;
	WR=1;
	while (!EEIF);
	WREN=0;
	return;
}

void EE_Char(unsigned char eesym, unsigned char x, unsigned char y) {
unsigned char line, ch, sym, i, j;

	for(line=0;line<5;line++) {
		sym=EERead((eesym<<2)+eesym+line);
		for (i=0;i<4;i++) {
				sym>>=1;
					ch = (C) ? 0x0F : 0x00;
				sym>>=1;
					ch += (C) ? 0xF0 : 0x00;
				SetY(i+(y<<2));
				SetX((x*24)+(line<<2));
				for (j=0;j<4;j++)
					Lcd_Write(DATA, ch);
//		Lcd_Write(DATA,0xFF);
			}
	}
return;
}

void EE_Char_Mid(unsigned char eesym, unsigned char x, unsigned char y) {
	unsigned char line,sym, j;
	
	for (line=0;line<5;line++) {
		SetX(x);
		SetY(y);

		sym=EERead(eesym*5+line);
		for(j=0;j<2;j++)
			Lcd_Write(DATA, conv[sym&0x0F]);
		SetX(x);
		SetY(++y);
		for(j=0;j<2;j++)
			Lcd_Write(DATA, conv[sym>>4]);
		y--;
		x+=2;
	}

return;
}

void EE_Char_Small(unsigned char eesym, unsigned char x, unsigned char y) {
	unsigned char line,sym;
	
	SetX(x);
	SetY(y);

	for (line=0;line<5;line++) {

		sym=EERead(eesym*5+line);
		Lcd_Write(DATA, sym);
	}

return;
}
void EE_Dec(unsigned int val, unsigned char x, unsigned char y) {
unsigned int k;
unsigned char show;
#ifdef _HIDE
	if (val<50) {
		EE_Char_Mid(0x0A,x*MID_WIDTH,y*HEIGHT);
		EE_Char_Mid(0x0B,(++x)*MID_WIDTH,y*HEIGHT);
		EE_Char_Mid(0x0B,(++x)*MID_WIDTH,y*HEIGHT);
		EE_Char_Mid(0x0B,(++x)*MID_WIDTH,y*HEIGHT);
		return;
        }
#endif
        show=0;
	k=val/1000;
	if (k) show=1;
	val=val-k*1000;
	if (!show) k=10;
	EE_Char_Mid(k,x*MID_WIDTH,y*HEIGHT);
	k=val/100;
	if (k) show=1;
	val=val-k*100;
	if (!show) k=10;
	EE_Char_Mid(k,(++x)*MID_WIDTH,y*HEIGHT);
	k=val/10;
	if (k) show=1;
	val=val-k*10;
	if (!show) k=10;
	EE_Char_Mid(k,(++x)*MID_WIDTH,y*HEIGHT);
	EE_Char_Mid(val,(++x)*MID_WIDTH,y*HEIGHT);
return;
}

void EE_Dec_Small(unsigned char val, unsigned char x, unsigned char y) {
unsigned int k;
unsigned char show;
// 	if (val<50) {
// 		EE_Char_Mid(0x0B,1,position);
// 		EE_Char_Mid(0x0B,2,position);
// 		EE_Char_Mid(0x0B,3,position);
// 		return;
// 	}
	show=0;
	k=val/100;
	if (k) show=1;
	val=val-k*100;
	if (!show) k=10;
	EE_Char_Small(k,x*WIDTH,y*HEIGHT);
	k=val/10;
	if (k) show=1;
	val=val-k*10;
	if (!show) k=10;
	EE_Char_Small(k,(++x)*WIDTH,y*HEIGHT);
	EE_Char_Small(val,(++x)*WIDTH,y*HEIGHT);
return;
}
