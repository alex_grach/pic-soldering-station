#ifndef __CONFIG_H__
#define __CONFIG_H__
#include "config.h"
#endif
#ifndef __LCD_H__
#define __LCD_H__
#include "lcd.h"
#endif
#ifndef __EEPROM_H__
#define __EEPROM_H__
#include "eeprom.h"
#endif
#ifndef __PID_H__
#define __PID_H__
#include "pid.h"
#endif
#ifndef __SS_H__
#define __SS_H__
#include "ss.h"
#endif

#ifdef _4K
#include "eeprom.c"
#include "pid.c"
#endif

//clear LCD
void Lcd_Clear(void){
unsigned char i,j;
//SetX(0);
//SetY(0);
Lcd_Write(CMD,0xAE); // disable display;
for(i=0;i<12;i++)
	for(j=0;j<72;j++)
Lcd_Write(DATA,0x00);
Lcd_Write(CMD,0xAF); // enable display;
	return;
}


// init LCD
void Lcd_Init(void){
unsigned char i;
cs = 0;
delay_s(2);			// 5mS so says the stop watch(less than 5ms will not work)
rst = 0;
delay_s(2);			// 5mS so says the stop watch(less than 5ms will not work)
rst = 1;
delay_s(2);			// 5mS so says the stop watch(less than 5ms will not work)
for (i=eelcd;i<eelcd+0x0D;i++) {
	Lcd_Write(CMD,EERead(i));
//	led=!led;
}

Lcd_Clear(); // clear LCD
Lcd_Write(CMD,0xA6); // invert display
delay_s(20);				// 1/2 Sec delay
return;
}

void Lcd_Write(unsigned char cd,unsigned char c){
unsigned char li;
	cs = 0;
	sclk = 0;
	sda = cd;
	sclk = 1;

for(li=0;li<8;li++){
		sclk = 0;
			if(c & 0x80)
				sda = 1;
			else
				sda = 0;
		sclk = 1;
		c <<= 1;
//		Delay10TCYx(20);
				}
		sclk=0;
		sda=0;
		cs = 1;
	return;
}


void SetX(unsigned char x){
	Lcd_Write(CMD,x & 0x0F);
	Lcd_Write(CMD,0x10 | ((x>>4)&0xF));
	return;
}

void SetY(unsigned char y){
	Lcd_Write(CMD,0xB0 + y);
	return;
}
